const config = require('../config');

/**
 * Defines all the routes
 * @param  {Object}   fastify  The global fastify server object
 * @returns {Object} Different per route
 */
async function routes(fastify) {
  fastify.get('/ping', {
    config,
    schema: {
      description: 'tests if service is alive',
      tags: ['user'],
      summary: 'tests if service is alive',
      querystring: {
        someKey: { type: 'string' },
      },
      response: {
        200: {
          type: 'object',
          properties: {
            response: { type: 'string' },
          },
        },
      },
    },
  }, async () => {
    fastify.log.info('[src#api#ping] Entering');

    const result = await fastify.countEvents();
    if (result > 0) {
      return { response: 'ok' };
    }

    throw new Error('Not ok');
  });

  fastify.get('/getEnergyProvider', {
    config,
    schema: {
      description: 'returns an energy provider',
      tags: ['user'],
      response: {
        200: {
          type: 'object',
          properties: {
            energy_provider: {
              type: 'object',
              properties: {
                name: { type: 'string' },
                uuid: { type: 'string' },
              }
            },
          },
        },
      },
    },
  }, async (request) => {
    fastify.log.info({ body: request.body }, '[src#api#getEnergyProvider] Entering');

    const pick = await fastify.pickEnergyProvider();
    fastify.log.info({ pick }, '[src#api#getEnergyProvider] Pick');

    // Drop message onto the bus - even if we will respond with direct energie
    if (config.run_with_bus) {
      await fastify.sendMessage(pick);
    }
    if (!config.run_with_bus) {
      await fastify.writeEvent('assignEnergyProvider', pick);
    }

    // If we have been told to always respond with direct eneergie - do it!
    if (config.direct_energie_only) {
      const de = await fastify.getDirectEnergie();
      return {
        energy_provider: de,
      };
    }

    if (!config.direct_energie_only) {
      return {
        energy_provider: pick,
      };
    }
    return {};
  });
}

module.exports = routes;
