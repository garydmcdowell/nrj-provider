require('make-promises-safe');
const helmet = require('fastify-helmet');
const fastify = require('fastify')({
  logger: true,
});

// Libs

// Global config params
const config = require('./config');

// Print routes
fastify.register(require('fastify-blipp'));

// Security
fastify.register(helmet, {
  hidePoweredBy: true,
  noCache: true,
});

// DB
fastify.register(require('./models'));

// Plugins
fastify.register(require('./plugins/app'));
fastify.register(require('./plugins/auth'));

if (config.run_with_bus) {
  fastify.register(require('./plugins/bus'));
}
fastify.register(require('./plugins/loadbalance'));

// Swagger doc
fastify.register(require('fastify-swagger'), {
  routePrefix: config.server.docPrefix,
  swagger: {
    info: {
      title: 'nrj-provider',
      description: 'responsible for the affectation of energy-provider-uuid during project creation',
      version: '1.0.0',
    },
    host: `http://localhost:${config.server.port}`,
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [
      { name: 'user', description: 'User related end-points' },
    ],
  },
  exposeRoute: true,
});

// Circuit breaker for async calls that can fail
fastify.register(require('fastify-circuit-breaker'), {
  threshold: 3, // default 5
  timeout: 5000, // default 10000
  resetTimeout: 5000, // default 10000
});

// Routes
fastify.register(require('./api'), { prefix: '/v1' });

// Hook it all together
const start = async () => {
  try {
    await fastify.listen(config.server.port, '0.0.0.0'); // This loads our plugins
    fastify.log.info(`server listening on ${fastify.server.address().port}`);

    // Output Routes
    fastify.blipp();

    // Generate swagger docs
    fastify.swagger();

    // App ok
    fastify.setHealth('ok');
  } catch (err) {
    fastify.log.error({ err }, 'server error');
    process.exit(1);
  }
};

start();

module.exports = fastify;
