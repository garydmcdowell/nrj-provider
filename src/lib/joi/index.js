const Joi = require('joi');

const objectIdExtend = require('./ObjectId.joi');

const joiExtends = [objectIdExtend];
const extendedJoi = Joi.extend(joiExtends);

module.exports = {
  Joi: extendedJoi
};
