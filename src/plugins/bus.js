const fp = require('fastify-plugin');
const workerlib = require('@skwirrel-mem/skwirrel-worker');
const amqp = require('@skwirrel-mem/node-amqp-bus');
const co = require('co');

const config = require('../config');
const { Joi } = require('../lib/joi');

const NRJMessage = {
  name: Joi.string().required(),
  uuid: Joi.string().required()
};


module.exports = fp(async (fastify) => {
  const client = await amqp.createClient(config.amqp.amqp_uri);
  await client.setupQueue(
    config.amqp.exchange,
    `${config.amqp.exchange}.${config.amqp.routing_key}`,
    config.amqp.routing_key
  );


  /**
   * Handles a message from myself!
   *
   * @param {Object} msg : the received message
   * @returns {Void} nada
   */
  function handle(msg) {
    fastify.log.info({ msg }, 'Storing');

    try {
      co(async () => {
        await fastify.writeEvent('assignEnergyProvider', msg);
      });
    } catch (error) {
      throw new Error(error);
    }
  }

  /**
   * Validates the incoming messages conforms to the structure we want
   *
   * @param {Object} msg : the received message
   * @returns {Object} Returns the msg for the handle stage
   */
  function validate(msg) {
    fastify.log.info({ msg }, 'Validating');

    const { error, returnMsg } = Joi.validate(msg, NRJMessage);
    if (error) {
      co(async () => {
        await fastify.quaratineMessage(msg);
      });

      throw error;
    }

    return returnMsg;
  }

  const worker = workerlib.createWorkers(
    [
      {
        handle,
        validate,
        routingKey: config.amqp.routing_key
      }
    ],
    {
      workerName: 'nrj-worker',
      amqpUrl: config.amqp.amqp_uri,
      exchangeName: config.amqp.exchange,
      queueName: config.amqp.queue_name
    },
    {
      channelPrefetch: 50,
      taskTimeout: 30000,
      processExitTimeout: 3000,
      channelCloseTimeout: 500
    }
  );
  fastify.log.info({ worker }, 'Listening for messages');

  worker.emitter.on('task.closed', () => {
    // Re-start to reconnect please
    fastify.setHealth('nok');
  });

  await worker.listen();

  fastify.decorate('sendMessage', async (message) => {
    try {
      await client.publish(
        config.amqp.exchange,
        config.amqp.routing_key,
        message
      );
    } catch (err) {
      throw new Error(err);
    }
  });
});
