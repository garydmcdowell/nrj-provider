const fp = require('fastify-plugin');

module.exports = fp(async (fastify) => {
  let health;
  fastify.decorate('getHealth', async () => health);

  fastify.decorate('setHealth', async (_health) => {
    health = _health;
  });
});
