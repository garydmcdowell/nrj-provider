const fp = require('fastify-plugin');
const loadbalance = require('loadbalance');

const config = require('../config');

module.exports = fp(async (fastify) => {
  let engine;
  let de;

  // Get the configuration
  const configuration = await fastify.getWeightings();

  fastify.decorate('createLoadBalancer', async (weightings) => {
    try {
      fastify.log.info({ weightings }, 'creating load balancer');
      engine = loadbalance.random(weightings);

    } catch (err) {
      throw new Error(err);
    }
  });

  fastify.decorate('pickEnergyProvider', async () => {
    try {
      return engine.pick();
    } catch (err) {
      throw new Error(err);
    }
  });

  fastify.decorate('setDirectEnergie', async (weightings) => {
    weightings.map(w => {
      if (w.object.name === 'Direct Énergie') {
        de = w.object;
      }
    });
  });

  fastify.decorate('getDirectEnergie', async () => de);

  // Setup the loadbalancer from config
  fastify.createLoadBalancer(configuration.weightings);

  if (config.direct_energie_only) {
    fastify.setDirectEnergie(configuration.weightings);
  }
});
