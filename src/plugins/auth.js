const fp = require('fastify-plugin');

const config = require('../config');

module.exports = fp(async (fastify) => {
  fastify.register(require('fastify-jwt'), { // eslint-disable-line global-require
    secret: config.secrets.jwt,
  });

  fastify.decorate('authenticate', async (request, reply) => {
    try {
      await request.jwtVerify();
    } catch (err) {
      reply.send(err);
    }
  });
});
