const fp = require('fastify-plugin');
const moment = require('moment-timezone');

const config = require('../config');

moment.tz.setDefault('Europe/Paris');

module.exports = fp(async (fastify) => {
  fastify.register(require('fastify-mongodb'), { // eslint-disable-line global-require
    // force to close the mongodb connection when app stopped
    // the default value is false
    url: config.db.url,
  });

  fastify.decorate('pickDirectEnergie', async () => {
    let result;
    try {
      const db = fastify.mongo.client.db('nrjprovider');

      const configuration = db.collection('configuration');

      result = await configuration.findOne({

      })
    } catch (err) {
      fastify.log.error({ err }, 'Error on collection read');
    }

    return result;
  });

  fastify.decorate('countEvents', async () => {
    let result;
    try {
      const db = fastify.mongo.client.db('nrjprovider');

      const collection = db.collection('events');

      result = await collection.countDocuments({});
    } catch (err) {
      fastify.log.error({ err }, 'Error on collection read');
    }

    return result;
  });

  fastify.decorate('getWeightings', async () => {
    let result;
    try {
      const db = fastify.mongo.client.db('nrjprovider');

      const configuration = db.collection('configuration');

      result = await configuration.findOne();
    } catch (err) {
      fastify.log.error({ err }, 'Error on collection read');
    }
    return result;
  });

  fastify.decorate('writeEvent', async (type, payload) => {
    let result;
    try {
      const db = fastify.mongo.client.db('nrjprovider');

      const responses = db.collection('events');

      const event = {
        type,
        metadata: {
          created_at: moment().toDate(),
        },
        payload
      };

      result = await responses.insertOne(event);
    } catch (err) {
      fastify.log.error({ err }, 'Error on collection read');
    }
    return result;
  });

  fastify.decorate('quaratineMessage', async (data) => {
    let result;
    try {
      const db = fastify.mongo.client.db('nrjprovider');

      const configuration = db.collection('quarantine');

      result = await configuration.insertOne(data);
    } catch (err) {
      fastify.log.error({ err }, 'Error on collection read');
    }
    return result;
  });
});
