FROM node:10.15.1-alpine

ARG NPM_TOKEN
ARG NODE_ENV
ENV NPM_TOKEN=${NPM_TOKEN}
ENV NODE_ENV=${NODE_ENV:-production}
WORKDIR /app
EXPOSE 3000

COPY .npmrc package.json package-lock.json ./
RUN npm install -production
ADD . .
