# Overview

nrj-provider is a microservice that is responsible for the affectation of energy-provider-uuid during project creation

This services fastify as a framework to keep it squeaky fast.

### Requirements

Install Node 10.15.1

For nvm users, just move to the project directory and run :

    nvm i

If you already have installed Node 10.15.1 before, just type:

    nvm use

## Run it
```
npm run start:dev
or
npm run start:prod
```

You can find the auto generated swagger documentation by default with http://localhost:PORT:doc

## Architecture

nrj-provider has 2 principle modes of operation - direct write to the DB or defer to a message queue. The version with the message queue is around x10 faster than the DB write (when the DB is remote). So it provides a performance boost which is important when you link a landing page to this service.

## Database

This micro service uses a database and it's health is checked through the api/ping route. This means if the connection is lost, the health check will fail and kubernetes will restart everything - hopefully getting a new connection.

YOU MUST HAVE an events collection in your DB which in itself is good practice - all micro services doing something with data should record, who, when, why and that is the events collection.

## Configuration

nrj-provider supports the following environment variables described below.

| Key  | Comment                        | Default |
| ---- | ------------------------------ | ------- |
| AMQPURI | The URI of the rabbitmq instance | 'amqp://guest:guest@localhost:5672' |
| AMQP_EXCHANGE | The exchange name | 'bus' |
| AMQP_BUS_TYPE | The type of message bus | 'topic' |
| DIRECT_ENERGIE_ONLY | Always return DE | false |
| DOC_URL_PREFIX | where to find the swagger documentation e.g. http://localhost:PORT/doc | doc |
| JWT_KEY | The token for authentication routes | BIG STRING |
| MONGODB_URL | MongoDB URL | 'mongodb://localhost:27017/nrjprovider' |
| NRJ_QUEUE_NAME | The queue name | 'bus' |
| NRJ_ROUTING_KEY | The routing key for selecting messages from the bus | 'nrj-event' |
| PORT | The port to use for the server | 3000    |
| RUN_WITH_BUS | Use the meesage bus (or DB) | false |

## Running locally

Run mongoDB in a local container

docker run -d --name my_mongo -p27017:27017 mongo

And a message bus (with admin interface)

docker run -d --name my_bus -p5672:p5672 -p15672:15672 rabbitmq:3-management

You can access the admin interface on 15672 (you will need to setup the exchange manually)

### Dockerizing

You should now be able to Dockerize you service with:

```
docker build --tag nrj-provider:tagofbuild .
```

and should now be able to push this to dockerhub with:

```
docker push nrj-provider:tagofbuild
```

Remember you will need to have run

```
docker login
```
