const MongoClient = require('mongodb').MongoClient;

const _ = require('lodash');
const fastify = require('../src/server');
const config = require('../src/config');
const { cleanConfig } = require('./fixtures/configurations');
const { cleanEvent } = require('./fixtures/events');

describe('Test the server', () => {
  let client;
  let db;

  beforeAll(async () => {
    client = new MongoClient(config.db.url);

    await client.connect();
    db = client.db('nrjprovider');

    try {
      await db.collection('events').deleteMany({});
      await db.collection('configuration').deleteMany({});
      await db.collection('quarantine').deleteMany({});

      // Insert the fixture
      delete cleanConfig._id;
      await db.collection('configuration').insertOne(cleanConfig);
    } catch(err) {
      console.log(err, 'Error on database');
    }
    await fastify.ready();
  });

  afterAll(async () => {
    await client.close();
  });

  test('Test the v1 ping route - ok', async () => {
    // Clear out data
    await db.collection('events').deleteMany({});

    // Insert the fixture
    delete cleanEvent._id;
    await db.collection('events').insertOne(cleanEvent);

    const response = await fastify.inject({
      method: 'GET',
      url: '/v1/ping',
    });
    expect(response.statusCode).toEqual(200);
  });

  test('Test the v1 ping route - nok', async () => {
    // Clear out data
    await db.collection('events').deleteMany({});

    const response = await fastify.inject({
      method: 'GET',
      url: '/v1/ping',
    });
    expect(response.statusCode).toEqual(500);
  });

  test('Test the v1 getEnergyProvider route', async () => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/v1/getEnergyProvider',
    });
    console.log(response.body);
    expect(response.statusCode).toEqual(200);
    expect(response.body).toContain('energy_provider');
  });

  test('Test the v1 getEnergyProvider route for direct energie only', async () => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/v1/getEnergyProvider',
    });
    expect(response.statusCode).toEqual(200);
    const body = JSON.parse(response.body);
    expect(body).toEqual({energy_provider: { name: 'Direct Énergie', uuid: 'e3c866b0-3ab7-445c-80a0-404b76069da9'}});
  });

  test('Test the swagger doc route, redirection', async () => {
    const response = await fastify.inject({
      method: 'GET',
      url: `${config.server.docPrefix}`,
    });
    expect(response.statusCode).toEqual(302); // redirects to swagger page
  });

  test('Test the swagger doc route', async () => {
    const response = await fastify.inject({
      method: 'GET',
      url: `${config.server.docPrefix}/static/index.html`,
    });
    expect(response.statusCode).toEqual(200);
  });

  test('Test quarantine', async () => {
    await fastify.quaratineMessage({
      hello: 'there',
    });

    // Read out data
    const quarantine = await db.collection('quarantine').findOne();
    expect(_.omit(quarantine, ['_id'])).toEqual({
      hello: 'there',
    });
  });
});
