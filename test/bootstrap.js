const  _ = require('lodash');
const assert = require('assert');

const config = require('../src/config');

assert(
  _.startsWith(config.db.url, 'mongodb://localhost'),
  'For security reasons, this test utility works only on a DB on localhost'
);
