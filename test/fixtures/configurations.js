const cleanConfig = {
  "weightings" : [
    {
      "object" : {
        "name" : "Direct Énergie",
        "uuid" : "e3c866b0-3ab7-445c-80a0-404b76069da9"
      },
      "weight" : 2
    },
    {
      "object" : {
        "name" : "Vattenfall",
        "uuid" : "89890898"
      },
      "weight" : 3
    },
    {
      "object" : {
        "name" : "Enis",
        "uuid" : "dseffezfze"
      },
      "weight" : 5
    }
  ]
};

module.exports = {
  cleanConfig
}
